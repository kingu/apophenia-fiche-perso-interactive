// Main dispatcher for the SVG file
// dispatch() is called onload in template.svg

    
function dispatch(e) {
    var filename = parent.location.pathname;
    var fileext = filename.substr(filename.lastIndexOf('.') + 1);
    loadScript((fileext === "svg") ? conf.svgLoadScript.standalone:conf.svgLoadScript.embedded);
}
