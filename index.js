window.onload = function() {
    $( "#simpleInputDialog" ).dialog({
	autoOpen: false
    });
    $( "#textInputDialog" ).dialog({
	autoOpen: false
    });
    $( "#d100Dialog").dialog({
	autoOpen: false
    });	
        $( "#exportDialog" ).dialog({
	autoOpen: false
    });	
    $( "#customDialog" ).dialog({
	autoOpen: false
    });
    $( document ).tooltip({
	position: { my: "left+15 center", at: "right center" }
    });
    if (location.protocol == "file:") {
	diag.message("fa-globe", "ATTENTION", "Cette application ne fonctionne qu'avec un serveur Web actif.  Veuillez d&eacute;marrer un serveur web et acc&eacute;der a cette page via localhost");
    }
    
    // Set current character
    let arg=location.search.substring(1);
    if (! isEmpty(arg)) {
	current.set( arg );
    }
    if (current.get() == conf.defaultCharacterId) {
	$("object").attr("Data", "splash.svg");
    } else {
	$("object").attr("Data", "template.svg?" + current.get())
    }
 
}

var dice = {
    rnd: function (f) {
	return Math.floor(Math.random() * Math.floor(f));
    },
    uni:   function(str) {
	let uniTbl = [ "&#x24EA;", "&#x2460;", "&#x2461;", "&#x2462;", "&#x2463;",
		       "&#x2464;", "&#x2465;", "&#x2466;", "&#x2467;", "&#x2468;" ];
	let tr = str.split("").map(function (x) { return uniTbl[parseInt(x)] }).join(""); 
	if (str == "100") {
	    return "&#x24EA;&#x24EA;";
	} else if (parseInt(str) < 10) {
	    return "&#x24EA;"+tr;
	} else {
	    return tr;
	}   
    },
    _d6:   function() { return this.rnd(6) },
    _d10:  function() { return this.rnd(10) },
    _d100: function() { return this.rnd(100) },
    d100:  function() {
	let x = this._d100();
	let xs = x.toString();
	return { result: x, str: xs, uni: this.uni(xs) }
    }
    
}

var diag = {
    value: function ( vname ) { return db.get(vname) },

    message: function (icon, title, message) {
	$( "#customDialog" ).dialog("option","title",title);
	$( "#customMain" ).html('<div><div style="width: 50px; float: left;"><i class="fa '+icon+' fa-3x" aria-hidden="true"></i></div>'+
				'<div style="margin-left: 70px;">' + message + '</div></div>');
	$( "#customDialog" ).dialog('option', 'buttons', {
	    "Compris!": function() {
		$(this).dialog( "close" );
	    }
	});
	$( "#customDialog" ).dialog("open");
    },
    
    create: function (opt) {
	let data = this.value(opt.vname);
	$( "#customDialog" ).dialog("option","title",opt.text);
	$( "#customMain" ).html(opt.content);
	if (opt.custom) opt.custom(data);
	$( "#customDialog" ).dialog('option', 'buttons', {
	    'Ok': function() {
		if (opt.onOk) {
		    opt.onOk($(this));
		} else {
		    t = opt.valueGetter();
		    opt.returnFunction(t);
		    db.set(opt.vname, t);
		    $(this).dialog('close');
		}},
	    Cancel: function() {
		$(this).dialog( "close" );
	    }
	});
	if (opt.width) $( "#customDialog" ).dialog( "option", "width", opt.width );
	if (opt.height) $( "#customDialog" ).dialog( "option", "height", opt.height );
	$( "#customDialog" ).dialog( "open");
    },

    dice: function (score) {
	let getSuccessLvl = function(score, mod, dice) {
	    let tbl = gamedata.niveauDeReussite[ (score % 20 == 0) ?  (score / 20) - 1 : Math.floor( score / 20 ) ]; 
	    if (dice.result <= score + mod) {
		if (dice.result <= tbl[0]) {
		    let audio = new Audio('sounds/orchestra.wav');
		    audio.play();   
		    return '<span style="color:blue">R&eacute;ussite critique!</a>';
		} else if (dice.result <= tbl[1]) {
		    return '<span style="color:green">R&eacute;ussite sp&eacute;ciale!</a>';
 		} else {
		    return '<span style="color:black">R&eacute;ussite!</a>';
		}
	    } else {
		if (dice.result < tbl[2]) {
		    return '<span style="color:purple">&Eacute;chec!</a>';
		} else {
		    let audio = new Audio('sounds/No Hope.wav');
		    audio.play();   
		    return '<span style="color:red">D&eacute;sastre !!!</a>';
		}
	    }
	}
	let reset = function () {
	    $( "#diceroll"  ).html("&#x24B6;&#x24B6");
	    $( "#dicerollresult"  ).html("");
	    $( "#d100mod" ).html(0)
	    $(".d100widget").show();
	}
	reset();
	$( "#d100Dialog" ).dialog("option","title", "Score: "+score+"%");
	$( "#d100score"  ).html(score);
	$( "#d100Dialog" ).dialog('option', 'buttons', {
	    'ROULER!': function() {
		let rn = Math.floor(Math.random() * Math.floor(29)) + 1;
		let audio = new Audio('sounds/dice-'+rn+'.wav');
		audio.play();
		audio.onended = function () {
		    $(".d100widget").hide();
		    let d = dice.d100();
		    $( "#diceroll" ).html(d.uni);
		    let mod = parseInt($( "#d100mod" ).html())
		    let modScore = d.result + mod;
		    $("#dicerollresult").html(getSuccessLvl(parseInt(score), mod, d));
		    $( "#d100Dialog" ).dialog('option', 'buttons', {
			'Reset': function() {
			    reset();
			    diag.dice(score);
			},
			Cancel: function() {
			    $(this).dialog( "close" );
			}
		    });
		}
	    },
	    Cancel: function() {
		$(this).dialog( "close" );
	    }
	});
	$( "#d100Dialog" ).on(`dialogclose`, function(ev, ui) {
	    reset();
	});
	$( "#d100Dialog" ).dialog("open");
	
    },	

    input: function ( vname, text, func ) { 
	let data = this.value(vname);
	$( "#simpleInputDialog" ).dialog("option","title",text);
	$( "#simpleDialogInput" ).val( (data) ? data : "" );
	$( "#simpleInputDialog" ).dialog('option', 'buttons', {
	    'Ok': function() {
		let di=$("#simpleDialogInput").val();
		func(di);
		db.set(vname, di);
		$(this).dialog('close');
	    },
	    Cancel: function() {
		$(this).dialog( "close" );
	    }
	});
	$( "#simpleInputDialog" ).dialog("open");
    },
    AGE: function (vname, text, func) {
	this.create( {
	    vname: vname,
	    text: text,
	    content: '<div><input type="radio" name="age" value="Adolescent">&nbsp;Adolescent (-16)</div>'+
		'<div><input type="radio" name="age" value="Jeune">&nbsp;Jeune (17-20)</div>'+
		'<div><input type="radio" name="age" value="Jeune adulte">&nbsp;Jeune adulte (21 - 30)</div>'+
		'<div><input type="radio" name="age" value="Adulte">&nbsp;Adulte (31 - 50)</div>'+
		'<div><input type="radio" name="age" value="Age mur">&nbsp;&Acirc;ge m&ucirc;r (51+)</div>',
	    custom: function(d) {
		let v = (d == undefined) ? "Jeune adulte" : d;
		$('input[name="age"][value="'+ v + '"]').attr('checked', true); }, 
	    valueGetter: function (v) { return $('input[name="age"]:checked').val(); },
	    returnFunction: func
	} );
    },
    
    TAI: function (vname, text, func) {
	this.create( {
	    vname: vname,
	    text: text,
	    content: '<div><input type="radio" name="tai" value="Petite">&nbsp;Petite</div>'+
		'<div><input type="radio" name="tai" value="Moyenne">&nbsp;Moyenne</div>'+
		'<div><input type="radio" name="tai" value="Grande">&nbsp;Grande</div>',
	    custom: function(d) {
		let v = (d == undefined) ? "Moyenne" : d;
		$('input[name="tai"][value="'+ v + '"]').attr('checked', true); }, 
	    valueGetter: function (v) { return $('input[name="tai"]:checked').val(); },
	    returnFunction: func
	} );
    },
    
    POI: function (vname, text, func) {
	let anorex = (gameRules.particuAvailable()) ? '<div class="particneg"><input type="radio" name="poi" value="Anorexique">&nbsp;Anorexique</div>' : '';
	let obese  = (gameRules.particuAvailable()) ? '<div  class="particneg"><input type="radio" name="poi" value="Ob�se">&nbsp;Ob�se</div>' : '';
	let bodybu = (gameRules.particuAvailable()) ? '<div  class="particpos"><input type="radio" name="poi" value="Culturiste">&nbsp;Culturiste</div>' : '';
	this.create( {
	    vname: vname,
	    text: text,
	    content: anorex + '<div><input type="radio" name="poi" value="l&eacute;ger">&nbsp;L&eacute;ger</div>'+
	    '<div><input type="radio" name="poi" value="Moyen">&nbsp;Moyen</div>'+
	    '<div><input type="radio" name="poi" value="Lourd">&nbsp;Lourd</div>'+ bodybu + obese,
	    custom: function(d) {
		let v = (d == undefined) ? "Moyen" : d;
		$('input[name="poi"][value="'+ v + '"]').attr('checked', true);
	    }, 
	    valueGetter: function (v) { return $('input[name="poi"]:checked').val(); },
	    returnFunction: func
	} );
    },
    
    MAI: function (vname, text, func) {
	this.create( {
	    vname: vname,
	    text: text,
	    content: '<div><input type="radio" name="mai" value="Droite">&nbsp;Droite</div>'+
		'<div><input type="radio" name="mai" value="Gauche">&nbsp;Gauche</div>'+
	    	'<div class="particpos"><input type="radio" name="mai" value="Ambidextre">&nbsp;Ambidextre</div>',
	    
	    custom: function(d) {
		let v = (d == undefined) ? "Droite" : d;
		$('input[name="mai"][value="'+ v + '"]').attr('checked', true);
	    }, 
	    valueGetter: function (v) { return $('input[name="mai"]:checked').val(); },
	    returnFunction: func
	} );
    },
    SEX: function (vname, text, func) {
	var data = this.value(vname);
	this.create( {
	    vname: vname,
	    text: text,
	    content: '<div><input list="sexlist" type="text" name="diag_SEX" id="diag_SEX" style="width:100%;"/></div>'+
		'<datalist id="sexlist"><option>Homme</option><option>Femme</option></datalist>',	    
	    custom: function(d) {
		
		$('#diag_SEX').val( (data) ? data : ""  );  
	    }, 
	    valueGetter: function (v) { return $('#diag_SEX').val(); },
	    returnFunction: func
	} );
    },
    
    ZOD: function (vname, text, func) {
	let lpad2 = function(num) {
	    return Array(Math.max(2 - String(num).length + 1, 0)).join(0) + num;
	}
	let zodL = [ "B&eacute;lier", "Taureau", "G&eacute;meau", "Cancer", "Lion", "Vierge",      
		     "Balance", "Scorpion", "Sagittaire", "Capricorne", "Verseau", "Poisson" ]
	let content = '<div style="margin-left: 15px; column-count: 2">';
	for (z in zodL) {
	    content += '<div><input type="radio" name="zod" value="' + z + '" title="'+ zodL[z] +'"/>&nbsp;&#98'
		+ lpad2(z) + ';&nbsp;'+zodL[z]+'</div>';
	}
	content += '</div>';
	this.create( {
	    vname: vname,
	    text: text,
	    width: 500,
	    content: content,
	    custom: function(d) {
		let v = (d == undefined) ? zodL[Math.floor(Math.random() * Math.floor(12))] : d;
		$('input[name="zod"][value="'+ v + '"]').attr('checked', true);
	    }, 
	    valueGetter: function (v) { return $('input[name="zod"]:checked').val(); },
	    returnFunction: func
	} );
    },
    
    attribs: function (vname, text, func) {
	this.create( {
	    vname: vname,
	    text: text,
	    content: '<div><input type="radio" name="attrib" value="1">&nbsp;1</div>'+
		'<div><input type="radio" name="attrib" value="2">&nbsp;2</div>'+
		'<div><input type="radio" name="attrib" value="3">&nbsp;3</div>'+
		'<div><input type="radio" name="attrib" value="4">&nbsp;4</div>'+
		'<div><input type="radio" name="attrib" value="5">&nbsp;5</div>',
	    custom: function(d) {
		let v = (d == undefined) ? "3" : d;
		$('input[name="attrib"][value="'+ v + '"]').attr('checked', true);
	    }, 
	    valueGetter: function (v) { return $('input[name="attrib"]:checked').val(); },
	    returnFunction: func
	} );
    },
    
    Arch: function (vname, text, func) {
	let content='<div><select id="arch">';
	for (a in gamedata.arch) {
	    content += "<option>"+a+"</option>";
	}
	content += "</select>";
	this.create( {
	    vname: vname,
	    text: text,
	    content: content,
	    custom: function(d) {
		let v = (d == undefined) ? "Scientifique" : d;
		$('input[name="arch"][value="'+ v + '"]').attr('checked', true);
	    },
	    valueGetter: function (v) { return $( "#arch option:selected" ).text(); },
	    returnFunction: func
	} );
    },
    
    traits: function (vname, text, func) {
	let zod = db.get("ZOD");
	if (! zod) {
	    	diag.message("fa-star-o", "Attention", "Vous devez en premier choisir un signe du zodiaque");
	} else {
	    let tL = gamedata.traits[zod][(vname == "TPN") ? 0 : 1];
	    content = 	html = '<div><select id="'+vname+'">';
	    for (tp in tL) {
		content += "<option>"+tL[tp]+"</option>";
	}
	content += "</select";
	    this.create( {
		vname: vname,
		text: text,
		content: content,
		custom: function(d) {
		    let v = (d == undefined) ? "Droite" : d;
		    $('input[name="'+vname+'"][value="'+ v + '"]').attr('checked', true);
		}, 
		valueGetter: function (v) { return $( "#"+vname+" option:selected" ).text(); },
		returnFunction: func
	    } );
	}
    },
    
    text: function ( vname, text, func ) { 
	let data = this.value(vname);
	$( "#textInputDialog" ).dialog("option","title",text);
	$( "#textDialogInput" ).val( (data) ? data : "" );
	$( "#textInputDialog" ).dialog('option', 'buttons', {
	    'Ok': function() {
		let di=$("#textDialogInput").val();
		$(this).dialog('close');
		func(di);
		db.set(vname, di);

	    },
	    Cancel: function() {
		$(this).dialog( "close" );
	    }
	});
	$( "#textInputDialog" ).dialog("open");
    },

    exporter: function() {
	var loading = function () {
	    $("#filetypeSelector").hide();
	    $("#exportLoader").show();
	    $("#exportDialog" ).dialog('option', 'buttons', {
		Cancel: function() {
		    $(this).dialog( "close" );
		}
	    });
	}
	var showData = function (data, downloadable=false) {
	    $("#processOutputTextarea").val(data);
	    $("#processOutputTextarea").ready(
		function(i,v) {
		    $("#exportLoader").hide();
		    $("#processOutput").show();
		    if (downloadable) {
			$("#exportDialog" ).dialog('option', 'buttons', {
			    "Download": function() {
				var blob = new Blob([data],{
				    type: "image/svg+xml;charset=utf-8;"
				});
				var url = window.URL.createObjectURL (blob);
				var a = document.createElement ('a');
				document.body.appendChild (a);
				a.href = url;
				a.download = current.get()+".svg";
				a.click ();
				window.URL.revokeObjectURL (url);
				document.body.removeChild (a);
				//navigator.msSaveBlob(blob, current.get()+".svg")
				//dbg("patate");
			    },
			    Cancel: function() {
				$(this).dialog( "close" );
			    }
			});
		    }
		});
	}
	$("#exportLoader").hide();
	$("#processOutput").hide();
	$("#processOutputTextarea").val('');
	$("#filetypeSelector").show();
	$( "#exportDialog" ).dialog("option","title","Export du personnage");
	$( "#exportDialog" ).dialog('option', 'buttons', {
	    APO: function() {
		loading();
		let data = db.encodeCkey();
		showData(data);
	    },
	    SVG: function() {
		loading();
		let data = new XMLSerializer().serializeToString($("object")[0].contentDocument);
		showData(data, true);
	    },
	    Cancel: function() {
		$(this).dialog( "close" );
	    }
	});
	$( "#exportDialog" ).dialog("open");
	
    }

}

function ask(vname, text, func) {
    switch(vname) {	
    case "AGE": case "TAI": case "POI": case "MAI": case "ZOD": case "SEX":
	diag[vname](vname, text, func);
	break;
    case "FOR": case "CON": case "INT": case "PER": case "POU": case "DEX": case "SOC":
	diag.attribs(vname, text, func);
	break;
    case "Arch":
	diag.Arch(vname, text,func);
	break;
    case "TPP": case "TPN":
	diag.traits(vname, text, func);
	break;
    case "Desc": case "Cont": case "Equip":
	diag.text(vname, text, func);
	break;
    default:	
	if (vname.startsWith("Comp")) {
	    console.log("adfasdf"+text)
	}
	diag.input(vname, text, func);
    }
}


function askComp(id, func) {
    console.log(id);
}


var toolbar = {
    
    selectChar: function() {
	let lk=db.listKeys();
	let content='<div><select id="charselect">';
	for (a in lk) {
	    content += "<option>"+lk[a]+"</option>";
	}
	content += "</select>";
	diag.create({
	    vname: "charselect",
	    text: "Personnages",
	    content: content,
	    valueGetter: function (v) { return $( "#charselect option:selected" ).text(); },
	    returnFunction: function(c) {
		$("object").attr("Data", "template.svg?"+c);
	    }
	})	
    },

    newChar: function () {
	diag.input("newChar", "IDdu personnage", function(c) {
	    $("object").attr("Data", "template.svg?"+c);
	});
    },

    importChar: function () {
	diag.create({
	    vname: "importChar",
	    text: "Importer un personnage",
	    content: '<div><div>Id: <input name="charname" id="charname"/></div>'+
		'<textarea id="chardata"></textarea></div>',
	    onOk: function(slf) {
		charName = $( "#charname" ).val();		
		db.decodeCkey( $( "#chardata" ).val(), charName );
		$("object").attr("Data", "template.svg?"+charName);
		slf.dialog('close');
	    }});	
    },

    exportChar: function () {
	diag.exporter();
    },

    createChar: function() {
	diag.message("fa-magic", "Cr&eacute;ation de personnage",
		     "D&eacute;sol&eacute;, l'assistant de cr&eacute;ation de personnage n'est pas encore implement&eacute;");
    },
    
    deleteChar: function () {
	if (current.get() == conf.defaultCharacterId) {
	    diag.message("fa-exclamation-triangle", "Attention", "Il n'y a pas de personnage a supprimer!");
	} else {
	    diag.create({
		vname: "deleteChar",
		text: "Suppression",
		content: 'Voules vous vraiment supprimer definitivement ce personnage?',
		onOk: function (slf) {
		    db.purge();
		    parent.document.getElementById("current_character").innerHTML = current.get();
		    $("object").attr("Data", "splash.svg");
		    slf.dialog('close'); }
	    });
	}
    },
    
    printChar: function() {
	$("object")[0].contentWindow.print();
    }
}


