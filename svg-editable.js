const SVG_EDITABLE = true;

function appendTextNodeToDocumentBox(id,text, x, y) { 
    deleteElementById(id);
    multilines.render(id, text, "_"+id, conf.fonts.calligraphy.name,conf.fonts.calligraphy.size.small);
}

function ask(vname, text, func) {
    return parent.ask(vname, text, func);
}

function getAttr(el, attr) {
    return parseFloat(el.getAttributeNS(null, attr));
}

function askAndInsert(vname,questionText, co) {
    ask(vname,questionText,
	function(answer) {
	    appendTextNodeToDocument(vname,answer, getAttr(co, 'x') + 2, getAttr(co, 'y') + getAttr(co, 'height') - 1 );
	    if (afterEffect[vname]) afterEffect[vname]();
	});
}

function askAndInsertBox(vname,questionText, co) {
    ask(vname,questionText,
	function(answer) {
	    appendTextNodeToDocumentBox(vname,answer, getAttr(co, 'x'), getAttr(co, 'y'), getAttr(co, 'width'), getAttr(co, 'height'));
	});
}

function askAndInsertComp(id, comp, co) {
    ask(id, comp, function(answer) {
	appendTextNodeToDocument(id, answer, getAttr(co, 'x'), getAttr(co, 'y') + 4, { type: "comp"});
    });	 
}

function setArch() { ask("Arch", "Archetype", function(answer) {
    competencesDump(answer);
}); }

function askAndInsertAttrib(attrib, questionText, co) {
    ask(attrib,questionText, function(answer) { insertAttrib(attrib, answer, getAttr(co, 'x'), getAttr(co, 'y'), getAttr(co, 'height')); });	 
}

var afterEffect = {
    ZOD: function() {
	document.getElementById("ZOD").textContent=gamedata.zod[db.get("ZOD")];
    }
}

function init() {
    let arg=location.search.substring(1)
    if (! isEmpty(arg)) current.set( arg );
    loadCharacter(current.get());
    let cgt = db.get("GT");
    changeGameType( (cgt) ? cgt : 1);
    parent.document.getElementById("current_character").innerHTML = current.get();
}

init();
