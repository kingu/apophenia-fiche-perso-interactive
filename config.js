// This file contains configuration options

const conf = {
    debug: true,
    defaultCharacterId: "aucun",
    svgLoadScript: {
	standalone:  "/svg-standalone.js",
	embedded:    "/svg-editable.js"
    },
    fonts: {
	regular: {
	    name: "Gentium Book Basic",
	    size: {
		small: "3px",
		normal: "5px",
		big: "10px",
	    }
	},
	calligraphy: {
	    name: "Lucida Calligraphy",
	    size: {
		small: "3px",
		normal: "4px",
		big: "5px"
	    }
	}
    }
}
