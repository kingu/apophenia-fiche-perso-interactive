// This file contains utility functions to be used by any js file
// and can either be called from html/jquery or svg/ecmascript)

var dbg = {
    cl: function(msg) {
	console.log(msg);
    },
    sc: function () {
	console.log(dbg.sc.caller.caller);
    }
}
// function dbg(msg, opt = { } ) {
//     if (opt.display === undefined) {
// 	opt.display =  true;
//     }
//     if (conf.debug && opt.display ) {
// 	if (opt.showCaller) console.log(dbg.caller.caller);
// 	if (msg) console.log(msg);
//     }
// }

function isEmpty(str) {
    return (!str || 0 === str.length);
}

// Use localStorage as a mini database system
var current = {
    get: function()    {
	let val = sessionStorage.getItem("APOPHENIA_CURRENT");
	return (val) ? val : conf.defaultCharacterId;
    },
    set: function(val) {
	sessionStorage.setItem("APOPHENIA_CURRENT",val) },
    purge: function() {
	sessionStorage.setItem("APOPHENIA_CURRENT", conf.defaultCharacterId) }
}    

var db = {
    listKeys: function()            {
	return Object.entries(localStorage).map(function(x) { return x[0] }).sort();
    },  
    getAll: function(dbn=null)         {
	return Object.assign({},JSON.parse(localStorage.getItem( (dbn) ? dbn : current.get() )));
    },
    encodeCkey: function(dbn=null)  {	
	return window.btoa(escape(localStorage.getItem((dbn) ? dbn : current.get())));
    },
    decodeCkey: function(ckey,dbn)    {
	let dec = unescape(window.atob(ckey));
	localStorage.setItem(dbn, dec);
	return dec;
    },    
    get: function(key, dbn=null)     {
	return this.getAll(dbn)[key];
    },
    set: function(key,val, dbn=null) {
	idb = this.getAll(dbn);
	idb[key] = val;
	localStorage.setItem((dbn) ? dbn : current.get(), JSON.stringify(idb)); 
	return val;		       
    },
    setAll: function(jsonData, dbn=null) {
	localStorage.setItem((dbn) ? dbn : current.get(), JSON.stringify(jsonData)); 
    },
    purge: function(dbn=null) {
	localStorage.removeItem((dbn) ? dbn : current.get());
	if (!dbn) current.purge();
    }, 
    foreach: function(func, dbn=null) {
	let all = this.getAll(dbn); 
	for (i in  all) {
	    func(i, all[i]);
	}
    },
    exists: function(dbn=false) {
	dbn = dbn || current.get();
	return (localStorage.getItem(dbn))
    },
    init: function(dbn=null) {
	dbn = dbn || current.get();
	if (db.exists(dbn)) {
	    dbg.cl("Database already exists!");
	} else {
            localStorage.setItem((dbn) ? dbn : current.get(), '{"GT":"1"}');
	}
    }
}

var gameRules = {
    particuAvailable: function () {
	let pp = gamedata.pointsParticu[db.get('GT')];
	return (pp == 0) ? false : pp; 
	
    }
}
