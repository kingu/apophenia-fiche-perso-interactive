// This files contains utilities function that are called from
// both the standalone or embedded scripts.

//const SVG = document.getElementsByTagName('svg')[0];


// SVG DOM manipulation stuff


function createNode(id, type, attribs=null) {
    let node = document.createElementNS('http://www.w3.org/2000/svg', type);
    node.setAttributeNS(null, 'id', id);
    if (attribs) addAttributes(node, attribs);
    return node
}

function createText(id, text, x, y , fontFamily, fontSize, style=null) {
    if (text) {
	let el = createNode(id, 'text');
	addAttributes(el, { x: x, y: y, "font-family": fontFamily, "font-size": fontSize, class: 'added' } );
	if (style) addAttributes(el, { 'style': style } );
	let txt = document.createTextNode(text);	     
	el.appendChild(txt);
	return el;
    }
}

function addAttributes(el, attribs) {
    for (a of  Object.entries(attribs)) {
	let isObj = (typeof(a[1]) == "object");
	el.setAttributeNS( (isObj) ? a[1][0] : null, a[0], (isObj) ? a[1][1] : a[1] );
    }
}

function appendNodeToDocument(node) {
    if (node == undefined) {
	dbg.sc();
    }
    deleteElementById(node.getAttribute("id"));
    document.getElementById("web").appendChild(node);
}

function loadScript(script) {
    let id = "script_" + escape(script);
    if (! document.getElementById(id)) {
	let el = document.createElementNS('http://www.w3.org/2000/svg', 'script');
	addAttributes(el, { "href": [ "http://www.w3.org/1999/xlink", script ], "id": id });
	appendNodeToDocument(el);
    }
}

function deleteElementById(id) { 
    el=document.getElementById(id);
    if (el) document.getElementById("web").removeChild(el);
}

function deleteElementByClassName(c) {
    let e = document.getElementsByClassName(c); 
    while (e.length > 0) e[0].remove();
}

var multilines = {
    getTextWidth: function (text, ff, fs) {
	appendTextNodeToDocument( "_tmp", text, 0, 0, { fontFamily: ff, fontSize: fs });
	let w = document.getElementById("_tmp").getBoundingClientRect().width;
	deleteElementById("_tmp");
	return w;
    },
    splitWords: function(text, maxWidth, ff, fs) {
	let wid = this.getTextWidth(text, ff, fs);
	if ( wid < maxWidth) {
	    return [text];
	} else {
	    let result=[];
	    let wordsL=text.split(" ");
	    let cl = "";
	    while (wordsL.length > 0) {
		let fl = cl + " " + wordsL[0];
		let w  = this.getTextWidth(fl, ff, fs);
		if (w < maxWidth) {
		    wordsL.shift();
		    cl = fl;
		} else {
		    result.push(cl);
		    cl="";
		}
	    }
	    result.push(cl);
	    return result;
	}
    },
    render: function (id, text, targetId, ff, fs, lineHeight="1.2em") {
	const target = document.getElementById(targetId);
	var wid = target.getBoundingClientRect().width;
	var lines = this.splitWords(text, wid, ff, fs);
	let svg = document.getElementsByTagName('svg')[0];
	let elem = document.createElementNS('http://www.w3.org/2000/svg', 'text');
	for (let l of lines) {
	    let txt = document.createTextNode(l.trim());
	    let ts  = document.createElementNS('http://www.w3.org/2000/svg', 'tspan');
	    ts.setAttributeNS(null, 'x', target.getAttribute("x"));
	    ts.setAttributeNS(null, 'dy', "1.2em");
	    ts.appendChild(txt);
	    elem.appendChild(ts);
	}
	addAttributes(elem, { id: id, y: target.getAttribute("y"), 'font-family': ff, 'font-size': fs });
	appendNodeToDocument(elem);
    }
}

function clearEditables() {
    deleteElementByClassName("attrib")
    deleteElementByClassName("edit")
}

function competencesDump(arch) {
    deleteElementById('archSelector');
    document.getElementById("archText").textContent = arch;
    let g = createNode('compBlock', 'g')
    createComptree(g, gamedata.arch[arch]['comptree'], 26, 122);
    appendNodeToDocument(g);
}

function createComptree(g, tree, x, y) {
    let skill      = tree[0];
    let baseskill  = (typeof(skill) != "string");
    let skillname  = (baseskill) ? skill.name + ' [' + skill.max + '] (' + skill.attrib + ')' : skill;
    let escaped    = escape(skillname);
    g.appendChild(createText("c_"+escaped, skillname, x, y, conf.fonts.regular.name, conf.fonts.regular.size.small));
    g.appendChild(createCompInputBlock(g, escaped, y));
    
    if (baseskill) {
	g.appendChild(createArrow(skill.max, x, y));
	y = createComptree(g, skill.comptree, x +  12, y + 6);
    }
    
    tree.shift();
    
    if (tree.length != 0) {
	y = createComptree(g, tree, x, y + 5);
    }
    return y;
}

function createArrow(max, x, y) {
    let ax = x + 9;
    let ay = y + 3
    let ag = createNode('arrowBlock', 'g')
    ag.appendChild(createText("A"+ ax +"_" + ay, "[" + max  + "]", ax - 9, ay + 1, conf.fonts.regular.name, conf.fonts.regular.size.small));
    var pth = "m "+ax+","+ay+" 1.963698,2.63549 -1.963698,2.635489 v -1.601964 h -1.090369 "+ 
	"q -0.335896,0 -0.599445,-0.268717 l -1.767328,-1.627802 q -0.222208,-0.19637 -0.222208,-0.635618 v -2.325432 "+ 
	"l 1.02319,0.516762 1.038693,-0.516762 v 2.795686 h 1.617467 z m 0.232543,4.568182 1.446936,-1.922357 "+
	"v -0.01034 l -1.446936,-1.93786 v 1.13171 h -3.317616 l 1.560623,1.431432 "+
	"q 0.211873,0.186035 0.434081,0.186035 h 1.322912 z m -3.689685,-3.431304 q 0,0.470254 0.563271,0.470254 "+
	"h 1.054196 v -2.45979 l -0.816485,0.41341 -0.800982,-0.41341 z";
    el = createNode('arrow', 'path', { d: pth, style: "stroke-width:0.26" });
    ag.appendChild(el);
    return ag;
}	 

function createCompInputBlock(g, id, y) {
    let ibg = createNode('ibg'+id, 'g');    
    let ibl = createNode('ibl'+id, 'path', { d: "m 103," + y + " 8.65717,-0.06",
					     style: "stroke-width:0.26;stroke:#000000;stroke-opacity:0.5;" });
    ibg.appendChild(ibl);
    ibg.appendChild(createText("pc"+ id, "%", 111, y, conf.fonts.regular.name, conf.fonts.regular.size.normal));
    let ibr = createNode('ibl'+id, 'rect', { width: 2.4, height: 2.5, x: 117, y: y - 2.5,
					     style: "fill:none;stroke:#000000;stroke-width:0.264583;stroke-linejoin:bevel;"+
    					     "stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" });
    ibg.appendChild(ibr);
    if (SVG_EDITABLE) {
	let box = createNode('_'+id, 'rect', { x: 102, y: y - 5, width: 8, height: 5,
					       style: "opacity:0.349359;fill:#9e0000;fill-opacity:0.0203252;stroke:#050000;"+
					       "stroke-width:0.433056;stroke-dasharray:0.433056, 0.866099;stroke-opacity:0.291411",
					       onclick: 'askAndInsertComp("' + id + '","' + unescape(id) + '", this)' }); 
	ibg.appendChild(box);
    }
    return ibg;
}

function changeGameType(gt) {
    db.set("GT", gt);
    deleteElementById('pathGtCircle');
    let pth = [
	'm 165.98034,37.490447 a 2.9242015,2.9242015 0 0 1 -3.94666,0.881722 2.9242015,2.9242015 0 0 1 '+
	    '-1.05358,-3.904291 2.9242015,2.9242015 0 0 1 3.85445,-1.223427 2.9242015,2.9242015 0 0 1 1.39092,3.797216',
	'm 174.56899,37.655547 a 2.9242015,2.9242015 0 0 1 -3.94665,0.881722 2.9242015,2.9242015 0 0 1 '+
	    '-1.05359,-3.904291 2.9242015,2.9242015 0 0 1 3.85445,-1.223427 2.9242015,2.9242015 0 0 1 1.39093,3.797216',
	'm 184.17709,37.490447 a 2.9242015,2.9242015 0 0 1 -3.94666,0.881722 2.9242015,2.9242015 0 0 1 '+
	    '-1.05358,-3.904291 2.9242015,2.9242015 0 0 1 3.85444,-1.223427 2.9242015,2.9242015 0 0 1 1.39093,3.797216' ];
    let el = createNode('pathGtCircle', 'path');
    el.setAttributeNS(null, 'd', pth[gt]);
    el.setAttributeNS(null, 'style', 'opacity:1;fill:none;fill-opacity:1;stroke:#a70000;stroke-width:0.56806;stroke-opacity:1');
    appendNodeToDocument(el);
    deleteElementById('CAP');
    appendNodeToDocument(createText('CAP',gamedata.seuilcap[gt],178,57, conf.fonts.regular.name, conf.fonts.regular.size.normal));
    setDerivedAttributes.all();
    document.getElementById("pointsParticu").textContent="("+ gamedata.pointsParticu[gt] + ")";
}

function insertAttrib(attrib, score, x, y, h, gt) {
    dbg.cl(attrib);
    dbg.sc();
    if (SVG_EDITABLE) db.set(attrib, score);
        appendNodeToDocument(createText(attrib, score, x+3, y+h-3, conf.fonts.calligraphy.name, conf.fonts.calligraphy.size.normal ));
       // appendNodeToDocument(createText(attrib, score, x+3, "patate", conf.fonts.calligraphy.name, conf.fonts.calligraphy.size.normal ));
    let a = parseFloat(score);
    appendNodeToDocument(
	createText(attrib+'P100', gamedata.atributsPourcentage[a-1], x+2, y+16, conf.fonts.calligraphy.name, conf.fonts.calligraphy.size.normal ));
    if (SVG_EDITABLE) {
	e = document.getElementById(attrib+'P100');
	e.onclick = function(evt) {  parent.diag.dice(evt.srcElement.textContent); };
    }
    switch(attrib) {
    case 'FOR':
	setDerivedAttributes.BD();
	break;
    case 'CON':
	setDerivedAttributes.PDV();
	break;
    case 'POU':
	setDerivedAttributes.PDM();
	break;
    }
}

var setDerivedAttributes = {
    BD: function() { 
	deleteElementById("BD");
	appendNodeToDocument(createText("BD", gamedata.bonusDegats[db.get("FOR")-1], 160, 209,
					conf.fonts.calligraphy.name, conf.fonts.calligraphy.size.normal));
    },
    PDV:  function() {
	deleteElementById("PDV");
	appendNodeToDocument(createText("PDV", db.get("CON") * gamedata.PDVmulti[db.get("GT")],
					190, 45, conf.fonts.calligraphy.name, conf.fonts.calligraphy.size.normal));
    },
    PDM:  function() {
	deleteElementById("PDM");
	appendNodeToDocument(createText("PDM", db.get("POU") * gamedata.PDMmulti[db.get("GT")],
					190, 64, conf.fonts.calligraphy.name, conf.fonts.calligraphy.size.normal));
    },
    all: function() { this.BD(); this.PDV(); this.PDM(); }
}

function setDefaultAttribs() {
    anl = document.getElementsByClassName("attrib");
    for (let atr of anl) {
	var id = atr.id.substring(1, 4);
	x = parseFloat(atr.getAttributeNS(null, 'x'));
	y = parseFloat(atr.getAttributeNS(null, 'y'));
	h = parseFloat(atr.getAttributeNS(null, 'height'));
	let ss = db.get(atr)
	let val = (ss) ? ss : 3;
	insertAttrib(id,val,x,y,h,1);
    }
}


function appendTextNodeToDocument(id,text, x, y, opt={}) {
    if (text) {
	deleteElementById(id);
	let txt = createText(id, text, x, y,
			     (opt.fontFamily) ? opt.fontFamily : conf.fonts.calligraphy.name,
			     (opt.fontSize)   ? opt.fontSize   : conf.fonts.calligraphy.size.small);
	if (SVG_EDITABLE && opt.type == "comp") {
	    txt.setAttributeNS(null, "onclick", "parent.diag.dice("+text+")");
	}
	appendNodeToDocument(txt);
	if (SVG_EDITABLE) {

	    if (id == "MAI") {
		if (text == "Ambidextre") {
		    addException("Ambidextre");
		} else {
		    removeException("Ambidextre");
		}
	    }
	    if (id == "POI") {
		if (text == "Ob�se" || text == "Anorexique") {
		    addException(text);
		} else {
		    removeException("Ob�se");
		    removeException("Anorexique");
		}
	    }
	}    
    }
}

function addException(excp, num = 0) {
    if (num > 3) alert("ERREUR: Il y a un maximum de quatre particularites");    
    let Ex = [document.getElementById("PAR0"),
	      document.getElementById("PAR1"),
	      document.getElementById("PAR2"),
	      document.getElementById("PAR3")]
    if (Ex[num]) {
	addException(excp, num + 1);
    } else {
	db.set("PAR"+num,excp);
	appendTextNodeToDocument("PAR"+num,
				 excp,
				 147,
				 120 + num * 6);
    }
}
function removeException(excp) {
    for (i = 0; i < 4; i++) {
	let pari="PAR"+i
	if (db.get(pari) == excp) {
	    db.set(pari,null)
	    deleteElementById(pari);
	}
    }
}
	
function loadCharacter(vname, ifNotExists = "create") {
    if (db.exists(vname)) {
	current.set(vname);
	db.foreach(function(k,v) {
	    var el = document.getElementById('_' + k );
	    switch (k) {
	    case "Arch":
		competencesDump(v);
		break
	    case "GT":
		changeGameType(v);
		break;
	    case "Desc": case "Cont": case "Equip":
		multilines.render(k,v,"_"+k,conf.fonts.calligraphy.name, conf.fonts.calligraphy.size.small);
		break;
	    case "FOR": case "CON": case "INT": case "PER": case "POU": case "DEX": case "SOC":
		x = parseFloat(el.getAttributeNS(null, 'x'));
		y = parseFloat(el.getAttributeNS(null, 'y'));
		h = parseFloat(el.getAttributeNS(null, 'height'));
		insertAttrib(k,v, x, y, h, db.get(vname, "GT"));
		break;
	    default:
		if (el) {
		    x = parseFloat(el.getAttributeNS(null, 'x'));
		    y = parseFloat(el.getAttributeNS(null, 'y'));
		    h = parseFloat(el.getAttributeNS(null, 'height'));
		    if (k == "NAM") {
			var fontSize=conf.fonts.calligraphy.size.big;
		    } else {
			var fontSize=conf.fonts.calligraphy.size.normal;
		    }
		    if (k == "ZOD") v = gamedata.zod[parseInt(v)];
		    appendTextNodeToDocument(k,v, x+2, y+h-1);
		}
	    }	
	}); 	
    } else {
	switch (ifNotExists) {
	case "create":
	    db.init(vname);
	    setDefaultAttribs();
	    break;
	case "ignore":
	    break;
	case "error":
	    alert('Le personnage n\'a pas ete touv�');
	    break;
	default:
	    ifNotExists();
	}
    }
}
