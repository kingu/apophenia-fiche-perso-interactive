const gamedata = {

    arch: {
	Scientifique:  {
	    archname: "Scientifique",
	    comptree: [{ name: "Science",
			 max: 40,
			 attrib: "INT",
			 comptree: [ "Physique", "Chimie", "Math", "Biologie", "Medecine" ]},
		       { name: "Pratique",
			 max: 40,
			 attrib: "SOC",
			 comptree: [ "Recherche", "Enseignement", "Communication", "Redaction", "Leadership" ]},
		       { name: "Pseudo Science",
			 max: 40,
			 attrib: "INT",
			 comptree: [ "Occultisme", "Ufologie", "QAnon", "Cryptozoologie", "Illuminatis, RC & NOM" ]}]
	},
	Guerrier: {
	    archname: "Guerrier",
	    comptree: [{ name: "Armes a feu",
			 max: 60,
			 attrib: "DEX",
			 comptree: [ "Fusil","Carabines","Mitraillette","Mitrailleuse","Fusil longue portee","Lance-roquette","Lance-flamme" ]},
		       { name: "Combat",
			 max: 40,
			 attrib: "DEX",
			 comptree: [ "Armes simples", "Bagarre", "Armes longues", "Armes articulees" ]},
		       { name: "Entrainement militaire",
			 max: 40,
			 attrib: "INT",
			 comptree: [ "Dicipline", "Commandement", "Strategie & Tactique", "Survie", "Diplomacie" ]}]
	},
	Informaticien: {
	    archname: "Informaticien",
	    comptree: [{ name: "Syst\u00e8me",
			 max: 30,
			 attrib: "INT",
			 comptree: [ "UNIX", "MS-Windows", "IOS", "Retro computer"] },
		       { name: "Programmation",
			 max: 60,
			 attrib: "INT",
			 comptree: [ "Math\u00e9matique", "OO (C++, C#, Java...)", "Scripting (Bash, Perl...)",
				     "Antique (Fortran, COBOL)", "Syst\u00e8me (Asm, Forth...)", "Fonctionnel (Scheme, Haskell...)" ]},
		       { name: "Informatique",
			 max: 40,
			 attrib:"INT",
			 comptree: [ "Bureautique", "Domotique", "Jeux Videos", "Internet" ]}]
	},
	Socialite: {
	    archname: "Socialite",
	    comptree: [{ name: "Socialisation",
			 max: 60,
			 attrib: "SOC",
			 comptree: ["Charme", "Diplomacie", "Persuasion", "Marchandage"] },
		       { name: "Connaissances",
			 max: 30,
			 attrib: "INT",
			 comptree: ["Politique", "Divertissement", "Logique", {  name: "Droit", max: 60, attrib: "INT", comptree: ["Local", "International"]}] },
		       { name: "Manipulation",
			 max: 40,
			 attrib: "INT",
			 comptree: ["Lecture du non-verbal", "Lectures des pens\u00e9es"] }]
	}
	// Athlete: {
	// 	archname: "Athlete",
	// 	comptree: [{ name: "Athletisme",
	// 		     max: 60,
	// 		     attrib: "FOR"
	
    },

    zod: [ "B�lier", "Taureau", "G�meau", "Cancer", "Lion", "Vierge",
	   "Balance", "Scorpion", "Sagittaire", "Capricorne", "Verseau", "Poisson" ],
    
    traits: [
	[ ["peu diplomate", "agressif", "col&eacute;rique"], ["ind&eacute;pendant", "direct", "combatif" ] ],
	[ ["possessif ", "jaloux", "intransigeant"],         ["prudent", "timide", "travailleur"] ],
	[ ["nerveux", "disper&#115;�", "hypersensible"],     ["d&eacute;brouillard", "curieux", "sociable"] ],
	[ ["vexable", "&eacute;motif", "fragile"],           ["g&eacute;n&eacute;reux", "attachant", "romantique"] ],
	[ ["autoritaire", "fier", "irritable"],              ["chaleureux", "cr&eacute;atif", "talentueux"] ],
	[ ["tatillon", "t&ecirc;tu", "lent"],                ["serviable", "d&eacute;licat", "gentil"] ],
	[ ["ind&eacute;cis", "&eacute;nigmatique","d&eacute;pendant"], ["courtois", "juste", "doux"] ],  
	[ ["extremiste", "apoph�niaque", "naif"],            ["tenace", "lucide", "anticonformiste"] ],
	[ ["comp&eacute;titif ", "instable", "impatient"],   ["chaleureux", "enthousiaste", "g&eacute;n&eacute;reux"] ],
	[ ["&eacute;go&iuml;ste", "froid", "introvertie"],   ["patient", "intr&eacute;pide", "tenace"] ],
	[ ["impr&eacute;visible", "individualiste", "d&eacute;sabus&eacute;"], ["curieux", "coop&eacute;ratifs", "adaptatif"]],
	[ ["anxieux", "fragile", "ind&eacute;cis"], ["d&eacute;vou&eacute;", "joueur", "intuitif"] ]	 
    ],

    armes: {
	impro: { name: "Arme improvis&eacute;e",
		 dexmod:  0,
		 comp: ["Bagarre", "Armes simples"],
		 deg: "1d3+1",
		 mains: 1 }
    },

    atributsPourcentage: [ 0, 20, 40, 60, 70 ],
    bonusDegats: [ '-1d6','-1d3','0','+1d3','+1d6'],
    PDVmulti: [3,4,5],
    PDMmulti: [3,4,5],
    seuilcap: [3,2,1],
    pointsParticu: [0,1,2],
    niveauDeReussite: [ [1, 5,96 ],
			[2,10,97 ],
			[3,15,98 ],
			[4,20,99 ],
			[5,25,100] ] 
}

