// This file is loaded only when the svg is loaded standalone
const SVG_EDITABLE = false;
//var archList = []
//var current_character = null;

function displayArchMenu() {
    var g = createNode('archSelector', 'g')
    var i = 0
    for (A in gamedata.arch) {
	let archname = gamedata.arch[A].archname;
	let e = createText(A, archname, 40, 140 + 12 * i, conf.fonts.regular.name, conf.fonts.regular.size.big, "cursor:pointer;")
	e.setAttributeNS(null, 'onclick', 'competencesDump("' + A + '")');
	g.appendChild(e);
	i++;
    }
    appendNodeToDocument(g);
}

// loading of character specified as argument
let arg=location.search.substring(1);
//dbg.cl(arg);
if (! isEmpty(arg)) {
    loadCharacter(arg);
} else {
    displayArchMenu();
}

clearEditables();





